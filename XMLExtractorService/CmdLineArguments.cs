﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace XMLExtractorService
{
    public class CmdLineArguments {
        [CommandLineParameter(Name = "source", ParameterIndex = 1, Required = true, Description = "Specifies the file or files to be copied.")]
        public string Source { get; set; } 
    }    
}
