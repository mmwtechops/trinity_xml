﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Xml;
using System.Configuration;


namespace XMLExtractorService
{
    public partial class Scheduler : ServiceBase
    {
        private Object thisLock = new Object();

        private Timer timer1 = null;

        public Scheduler()
        {
            Program.log.Info("Scheduler()");
            InitializeComponent(); 
        }

        protected void Actions()
        {
            DBOperation source = new DBOperation();
            String sourceConnection = "Data Source=" + Program.sourceServer + ";Initial Catalog=" + Program.sourceDB + ";Integrated Security=SSPI";          
            
            source.SetConnection(sourceConnection);
            if (!source.OpenConnection()) return;

            DataTable dt = new DataTable();
            Boolean sqlQryCheck = source.ExecuteSQLCommandWithResults("Select * from TRI_MessageQueue WITH (NOLOCK) WHERE IsProcessed = 0 ", ref dt);
            if (!sqlQryCheck) return;

            String msg = dt.Rows.Count.ToString();
            Program.log.Info(msg + " input records to be processed...");

            List<DataResult> dataResult = new List<DataResult>();

            int i = 0;
            foreach (DataRow r in dt.Rows)
            {
                DataResult dResult = new DataResult();

                dResult.ID = (int)dt.Rows[i]["ID"];

                // XML Content
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(dt.Rows[i]["PostXML"].ToString());

                // Used for debugging purpose only
                // String tempXmlFile = "Temp" + i + ".xml";
                // doc.Save(tempXmlFile);

                TrinityXML myXml = new TrinityXML();

                myXml.ReadXMLSection(doc, "NumberOfSections");
                ListWithDuplicates numSectionXmlVal = myXml.ValueList;
                dResult.NumberOfSections = Convert.ToInt32(numSectionXmlVal[0].Value);
                Program.log.Debug(dResult.NumberOfSections);

                myXml.ReadXMLSection(doc, "QuotedDateUTC");
                ListWithDuplicates quotedDateXmlVal = myXml.ValueList;
                dResult.QuotedDateUTC = Convert.ToDateTime(quotedDateXmlVal[0].Value);
                Program.log.Debug(dResult.QuotedDateUTC);

                myXml.ReadXMLSection(doc, "AcceptedDateUTC");
                ListWithDuplicates acceptedDateXmlVal = myXml.ValueList;
                dResult.AcceptedDateUTC = Convert.ToDateTime(acceptedDateXmlVal[0].Value);
                Program.log.Debug(dResult.AcceptedDateUTC);

                myXml.ReadXMLSection(doc, "Workflow");
                ListWithDuplicates workXmlVal = myXml.ValueList;
                dResult.Workflow = workXmlVal[0].Value;
                Program.log.Debug(dResult.Workflow);

                myXml.ReadXMLSection(doc, "Title");
                ListWithDuplicates titleXmlVal = myXml.ValueList;
                dResult.Title = titleXmlVal[0].Value;
                Program.log.Debug(dResult.Title);

                myXml.ReadXMLSection(doc, "JobType");
                if (myXml.ValueList.Count > 0)
                {
                    ListWithDuplicates jobTypeXmlVal = myXml.ValueList;
                    dResult.JobType = jobTypeXmlVal[0].Value;
                    Program.log.Debug(dResult.JobType);
                }

                myXml.ReadXMLSection(doc, "Customer");
                ListWithDuplicates customerXmlVal = myXml.ValueList;
                dResult.ClientName = customerXmlVal[1].Value;
                Program.log.Debug(dResult.ClientName);

                myXml.ReadXMLSection(doc, "AccountManager");
                ListWithDuplicates acctManagerXmlVal = myXml.ValueList;
                dResult.AcctManager = acctManagerXmlVal[1].Value;
                Program.log.Debug(dResult.AcctManager);

                myXml.ReadXMLSection(doc, "JobNumber");
                ListWithDuplicates result1 = myXml.ValueList;
                String jobNumberStr = result1[0].Value;
                Program.log.Debug(jobNumberStr);
                // Extract the Run number if any
                int found = jobNumberStr.IndexOf(".");
                Int64 runNum = 0;
                if (found > 0)
                {
                    dResult.JobNo = jobNumberStr.Substring(0, found);
                    runNum = Convert.ToInt64(jobNumberStr.Substring(found + 1));
                    dResult.JobFrequency = 'R';
                }
                else
                {
                    dResult.JobNo = jobNumberStr;
                    dResult.JobFrequency = 'S';
                }
                dResult.RunNo = runNum;
                Program.log.Info(dResult.RunNo);

                myXml.ReadXMLSection(doc, "Product/Operations");
                ListWithDuplicates result2 = myXml.ValueList;
                dResult.OperationList = dResult.PopulateOperations(result2);

                myXml.ReadXMLSection(doc, "Packs");
                ListWithDuplicates result3 = myXml.ValueList;
                dResult.RunSectionList = dResult.PopulateRunSections(result3);

                dataResult.Add(dResult);                              
               
                ++i;
            }

            if (dt.Rows.Count > 0)
            {
                String jobSP = "up_InsertUpdate_Job";
                String jobRunSP = "up_InsertUpdate_JobRun";
                String jobRunOpsSP = "up_InsertUpdate_JobRunOperation";
                String jobRunSectionSP = "up_InsertUpdate_JobRunSection";

                DBOperation target = new DBOperation();
                String targetConnection = "Data Source=" + Program.targetServer + ";Initial Catalog=" + Program.targetDB + ";Integrated Security=SSPI";
                target.SetConnection(targetConnection);
                target.OpenConnection();
               
                target.ExecuteStoredProcJob(jobSP, dataResult);
                target.ExecuteStoredProcJobRun(jobRunSP, dataResult);
                target.ExecuteStoredProcJobRunOps(jobRunOpsSP, dataResult);
                target.ExecuteStoredProcJobRunSection(jobRunSectionSP, dataResult);

                // Close the connection to the destination server/database
                target.CloseConnection();

                foreach (DataResult d in dataResult)
                {
                    // Update the IsProcessedFlag for the jobxml record in the Source  
                    String cmdSqlProcessedFlag = "Update TRI_MessageQueue " +
                            " SET IsProcessed = 1 WHERE ID = " + d.ID; 
                    source.ExecuteSQLCommand(cmdSqlProcessedFlag);
                }
            }

            // Close the connection to the source database
            source.CloseConnection();
        }


        protected override void OnStart(string[] args)
        {
            timer1 = new Timer();
            this.timer1.Interval = 1000 * 60 * 2; //fires every 2mins
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            timer1.Enabled = true;           

            Program.Start(args);
        }

        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            timer1.Enabled = false;

            lock (thisLock)
            {
                Actions();
            }

            timer1.Enabled = true;         
        }

        protected override void OnStop()
        {
            timer1.Enabled = false;          

            Program.Stop();
        }
    }
}
