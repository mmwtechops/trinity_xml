﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CmdLine;

namespace XMLExtractorService
{
    class Options
    {
        [CommandLineParameter(Name = "source", ParameterIndex = 1, Description = "Specifies the source server.")]
        public string Source { get; set; } 

        [CommandLineParameter(Name = "destination", ParameterIndex = 2, Description = "Specifies the destination server.")]
        public string Destination { get; set; } 
  }
}
   
