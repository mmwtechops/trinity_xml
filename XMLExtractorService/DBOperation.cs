﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace XMLExtractorService
{
    partial class DBOperation
    {
        private SqlConnection _currentConnection;
        private String _connectionStr;

        public void SetConnection(String connStr)
        {
            _currentConnection = new SqlConnection(connStr);
            _connectionStr = connStr;
        }

        public String GetConnectionString()
        {
            return _connectionStr;
        }

        public SqlConnection GetCurrentConnection()
        {
            return _currentConnection;
        }

        public bool OpenConnection()
        {
            try
            {
                Program.log.Info("Opening connection to : " + _connectionStr);
                _currentConnection.Open();
            }
            catch (SqlException e)
            {
                Program.log.Fatal(e.Message);
                return false;
            }

            return true;
        }

        public void CloseConnection()
        {
            Program.log.Info("Closing connection to : " + _connectionStr);
            _currentConnection.Close();
        }

        public void ExecuteSQLCommand(String cmd)
        {
            Program.log.Info("Executing SQL command : " + cmd);

            SqlTransaction transaction;
            transaction = _currentConnection.BeginTransaction("");

            using (SqlCommand sqlcmd = new SqlCommand(cmd, _currentConnection))
            {
                sqlcmd.Transaction = transaction;
                try
                {
                    sqlcmd.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Program.log.Fatal(ex.Message);

                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception exRollback)
                    {
                        Program.log.Fatal(exRollback.Message);
                    }

                    CloseConnection();
                }

            }
        }

        public /*void*/ Boolean ExecuteSQLCommandWithResults(String cmd, ref DataTable dTable)
        {
            Program.log.Info("Executing SQL command : " + cmd);

            using (SqlCommand sqlcmd = new SqlCommand(cmd, _currentConnection))
            {
                try
                {
                    SqlDataReader reader;

                    sqlcmd.CommandType = CommandType.Text;
                    reader = sqlcmd.ExecuteReader();
                    
                    dTable.Load(reader);
                } catch(SqlException e)
                {
                    Program.log.Fatal(e.Message);
                    CloseConnection();
                    
                    return false;
                }
            }
            return true;
        }

        public void ExecuteStoredProcJobRun(String sqlCmd, List<DataResult> pList)
        {
            Program.log.Info("Executing SQL command : " + sqlCmd);

            foreach (DataResult p in pList)
            {
                //Program.log.Info(p.RunNo);
                using (SqlCommand sqlcmd = new SqlCommand(sqlCmd))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;

                    sqlcmd.Connection = _currentConnection;
                    sqlcmd.Parameters.Add("@JobNumber", SqlDbType.VarChar, 50).Value = p.JobNo;
                    sqlcmd.Parameters.Add("@RunNumber", SqlDbType.Int).Value = p.RunNo;
                    sqlcmd.Parameters.Add("@JobRunStatus", SqlDbType.VarChar, 50).Value = DBNull.Value;
                    sqlcmd.Parameters.Add("@JobRunNote", SqlDbType.VarChar, 50).Value = DBNull.Value;
                    sqlcmd.Parameters.Add("@SourceCode", SqlDbType.VarChar, 50).Value = "Trinity";                                   

                    SqlTransaction transaction;
                    transaction = _currentConnection.BeginTransaction("JobRunTransaction");
                    sqlcmd.Transaction = transaction;

                    try
                    {
                        sqlcmd.ExecuteNonQuery();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Program.log.Fatal(p.JobNo);
                        Program.log.Fatal(p.RunNo.ToString());
                        
                        Program.log.Fatal(ex.Message);

                        try
                        {
                            transaction.Rollback();
                        }
                        catch (Exception exRollback)
                        {
                            Program.log.Fatal(exRollback.Message);
                        }

                        CloseConnection();
                    }
                }
            }
        }

        public void ExecuteStoredProcJob(String sqlCmd, List<DataResult> pList)
        {
            Program.log.Info("Executing SQL command : " + sqlCmd);

            foreach (DataResult p in pList)
            {
                using (SqlCommand sqlcmd = new SqlCommand(sqlCmd))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;

                    sqlcmd.Connection = _currentConnection;
                    sqlcmd.Parameters.Add("@JobNumber", SqlDbType.VarChar, 50).Value = p.JobNo;
                    sqlcmd.Parameters.Add("@Client", SqlDbType.VarChar, 255).Value = p.ClientName;
                    if (p.RunNo > 0)
                    {
                        sqlcmd.Parameters.Add("@JobFrequency", SqlDbType.Char).Value = 'R';
                    }
                    else
                    {
                        sqlcmd.Parameters.Add("@JobFrequency", SqlDbType.Char).Value = 'S';
                    }
                    sqlcmd.Parameters.Add("@AccContact_ID", SqlDbType.Int).Value = DBNull.Value;
                    sqlcmd.Parameters.Add("@AcctManagerName", SqlDbType.VarChar, 255).Value = p.AcctManager;                   
                    sqlcmd.Parameters.Add("@JobNote", SqlDbType.VarChar, 255).Value = DBNull.Value;
                    sqlcmd.Parameters.Add("@SourceCode", SqlDbType.VarChar, 50).Value = "Trinity";                   
                       
                    SqlTransaction transaction;
                    transaction = _currentConnection.BeginTransaction("JobTransaction");

                    try
                    {
                        sqlcmd.Transaction = transaction;

                        sqlcmd.ExecuteNonQuery();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Program.log.Fatal(p.JobNo);
                        Program.log.Fatal(p.ClientName);
                        
                        Program.log.Fatal(ex.Message);

                        try
                        {
                            transaction.Rollback();
                        }
                        catch (Exception exRollback)
                        {
                            Program.log.Fatal(exRollback.Message);
                        }
                    }
                }
            }
        }

        public void ExecuteStoredProcJobRunOps(String sqlCmd, List<DataResult> pList)
        {
            Program.log.Info("Executing SQL command : " + sqlCmd);

            //Program.log.Info("Count = " + pList.Count);
            foreach (DataResult p in pList)
            {
                //Program.log.Info("Count1 = " + p.OperationList.Count);
                foreach (OperationStruct o in p.OperationList)
                {
                    //Program.log.Info("Count2 = " + o._OpParamList.Count);
                    foreach (OpParameterStruct param in o._OpParamList)
                    {
                        using (SqlCommand sqlcmd = new SqlCommand(sqlCmd))
                        {
                            sqlcmd.CommandType = CommandType.StoredProcedure;

                            sqlcmd.Connection = _currentConnection;
                            sqlcmd.Parameters.Add("@JobNumber", SqlDbType.VarChar, 50).Value = p.JobNo;
                            sqlcmd.Parameters.Add("@RunNumber", SqlDbType.Int).Value = p.RunNo;
                            sqlcmd.Parameters.Add("@OperationKey", SqlDbType.VarChar, 50).Value = o.OperationKey;
                            sqlcmd.Parameters.Add("@OperationName", SqlDbType.VarChar, 50).Value = o.Name;
                            sqlcmd.Parameters.Add("@EstimatedHours", SqlDbType.Decimal).Value = o.EstRunTime;

                            sqlcmd.Parameters.Add("@ParamKey", SqlDbType.Int).Value = param.key;
                            sqlcmd.Parameters.Add("@ParamName", SqlDbType.VarChar, 50).Value = param.name;
                            sqlcmd.Parameters.Add("@ParamQuestion", SqlDbType.VarChar, 255).Value = param.question;

                            SqlTransaction transaction;
                            transaction = _currentConnection.BeginTransaction("JobRunOpTransaction");

                            try
                            {
                                sqlcmd.Transaction = transaction;                            
                                sqlcmd.ExecuteNonQuery();
                                transaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                Program.log.Fatal(p.JobNo);
                                Program.log.Fatal(o.OperationKey);
                                Program.log.Fatal(param.key);
                                Program.log.Fatal(param.name);
                                Program.log.Fatal(param.question);

                                Program.log.Fatal(ex.Message);

                                try
                                {
                                    transaction.Rollback();
                                }
                                catch (Exception exRollback)
                                {
                                    Program.log.Fatal(exRollback.Message);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void ExecuteStoredProcJobRunSection(String sqlCmd, List<DataResult> pList)
        {
            Program.log.Info("Executing SQL command : " + sqlCmd);

            //Program.log.Info("Count = " + pList.Count);
            foreach (DataResult p in pList)
            {
                foreach (RunSectionStruct o in p.RunSectionList)
                {
                    //Program.log.Info("Count2 = " + p.RunSectionList.Count);                 
                    using (SqlCommand sqlcmd = new SqlCommand(sqlCmd))
                    {
                        sqlcmd.CommandType = CommandType.StoredProcedure;

                        sqlcmd.Connection = _currentConnection;
                        sqlcmd.Parameters.Add("@JobNumber", SqlDbType.VarChar, 50).Value = p.JobNo;
                        sqlcmd.Parameters.Add("@RunNumber", SqlDbType.Int).Value = p.RunNo;
                        sqlcmd.Parameters.Add("@SectionKey", SqlDbType.VarChar, 50).Value = o.sectionKey;
                        sqlcmd.Parameters.Add("@SectionName", SqlDbType.VarChar, 50).Value = o.sectionName;
                        sqlcmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = o.quantity;

                        SqlTransaction transaction;
                        transaction = _currentConnection.BeginTransaction("JobRunSectionTransaction");

                        try
                        {
                            sqlcmd.Transaction = transaction;                        
                            sqlcmd.ExecuteNonQuery();
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            Program.log.Fatal(p.JobNo);
                            Program.log.Fatal(p.RunNo);
                            Program.log.Info(o.sectionKey);
                            Program.log.Info(o.sectionName);
                            Program.log.Info(o.quantity);

                            Program.log.Fatal(ex.Message);

                            try
                            {
                                transaction.Rollback();
                            }
                            catch (Exception exRollback)
                            {
                                Program.log.Fatal(exRollback);
                            }
                        }
                    }              
                }
            }
        }
    }
}
