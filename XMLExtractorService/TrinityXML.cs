﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;


namespace XMLExtractorService
{
    public class ListWithDuplicates : List<KeyValuePair<string, string>>
    {
        public void Add(string key, string value)
        {
            var element = new KeyValuePair<string, string>(key, value);
            this.Add(element);
        }
    }

    class TrinityXML
    {
        private ListWithDuplicates _valueList;

        public ListWithDuplicates ValueList
        {
            get { return _valueList; }
            set { _valueList = value; }
        }

        public void ReadXMLSection(XmlDocument doc, String section)
        {
            _valueList = new ListWithDuplicates();

            XmlNode root = doc.SelectSingleNode("Job/" + section);
            ReadXML(root, section);
        }

        private void ReadXML(XmlNode root, String section)
        {
            if (root is XmlElement)
            {
                if (root.HasChildNodes)
                {
                    ReadXML(root.FirstChild, section);

                    _valueList.Add(root.Name, root.FirstChild.InnerText);
                }
                if (root.NextSibling != null)
                {
                    if (root.Name != section)
                        ReadXML(root.NextSibling, section);
                    else
                        return;
                }

            }
        }
    }
}
