﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLExtractorService
{
    struct OperationStruct
    {
        public String OperationKey;
        public String Name;
        public decimal EstRunTime;

        public List<OpParameterStruct> _OpParamList;
    }

    struct OpParameterStruct
    {
        public int key;
        public String name;
        public String question;

        public void Clear()
        {
            key = 0;
            name = "";
            question = "";
        }
    }

    struct RunSectionStruct
    {
        public String sectionKey;
        public String sectionName;
        public int quantity;

        public void Clear()
        {
            sectionKey = "";
            sectionName = "";
            quantity = 0;
        }
    }

    /* NOT USED atm
    struct ContactStruct
    {
        String Name;
        String Phone;
        String Mobile;
        String Email;
    }
      
    struct AddressStruct
    {
        String Name;
        String Address1;
        String Address2;
        String Address3;
        String City;
        String PostCode;
        String State;
        String Country;
    }
  

    struct ClientStruct
    {
        String Number;
        String Name;
        AddressStruct ClientAddress;
        ContactStruct Contacts;
    }
    */

    class DataResult
    {
        private int _ID;
        private String _JobNo;
        private Int64 _RunNo;
        private char _JobFrequency;
        private int _NumberOfSections;
        private DateTime _QuotedDateUTC;
        private DateTime _AcceptedDateUTC;
        private String _Workflow;
        private String _Title;
        private String _JobType;

        private String _ClientName;

        private String _AcctManager;

        // private ClientStruct _Client;
        private List<OperationStruct> _OperationList;

        private List<RunSectionStruct> _RunSectionList;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public String JobNo
        {
            get { return _JobNo; }
            set { _JobNo = value; }
        }

        public Int64 RunNo
        {
            get { return _RunNo; }
            set { _RunNo = value; }
        }

        public char JobFrequency
        {
            get { return _JobFrequency; }
            set { _JobFrequency = value; }
        }

        public int NumberOfSections
        {
            get { return _NumberOfSections; }
            set { _NumberOfSections = value; }
        }

        public DateTime QuotedDateUTC
        {
            get { return _QuotedDateUTC; }
            set { _QuotedDateUTC = value; }
        }

        public DateTime AcceptedDateUTC
        {
            get { return _AcceptedDateUTC; }
            set { _AcceptedDateUTC = value; }
        }

        public String Workflow
        {
            get { return _Workflow; }
            set { _Workflow = value; }
        }

        public String Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public String JobType
        {
            get { return _JobType; }
            set { _JobType = value; }
        }

        public String ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        public String AcctManager
        {
            get { return _AcctManager; }
            set { _AcctManager = value; }
        }

        /*
        public ClientStruct Client
        {
            get { return _Client; }
            set { _Client = value; }
        }
        */

        public List<OperationStruct> OperationList
        {
            get { return _OperationList; }
            set { _OperationList = value; }
        }

        public void AddOperation(OperationStruct op)
        {
            _OperationList.Add(op);
        }

        public List<RunSectionStruct> RunSectionList
        {
            get { return _RunSectionList; }
            set { _RunSectionList = value; }
        }

        public List<OperationStruct> PopulateOperations(ListWithDuplicates result2)
        {
            Program.log.Debug("Calling PopulateOperations()..");

            List<OperationStruct> retList = new List<OperationStruct>(); ;

            String opKey = "";
            String name = "";
            String machine = "";
            String estmrt = "";
            String estrun = "";
            decimal esthours = 0.0m;

            List<OpParameterStruct> opParamList = new List<OpParameterStruct>();
            
            for (int ctr = 0; ctr < result2.Count; ++ctr)
            {
                if (result2[ctr].Key == "OperationKey")
                {
                    opKey = result2[ctr].Value;
                }
                else if (result2[ctr].Key == "Name")
                {
                    name = result2[ctr].Value;
                }
                else if (result2[ctr].Key == "Machine")
                {
                    machine = result2[ctr].Value;
                }
                else if (result2[ctr].Key == "EstMRTimeHHMM")
                {
                    estmrt = result2[ctr].Value;
                }
                else if (result2[ctr].Key == "EstRunTimeHHMM")
                {
                    estrun = result2[ctr].Value;

                    int colonIdx = estrun.IndexOf(":");
                    Int64 hours = Convert.ToInt64(estrun.Substring(0, colonIdx));

                    int len = estrun.Length;
                    Int64 mins = Convert.ToInt64(estrun.Substring(colonIdx + 1));

                    decimal hr = (decimal)hours;
                    decimal min = (decimal)mins;

                    esthours = (hr + min / 60);
                }
                else if (result2[ctr].Key == "ParameterKey")
                {
                    OpParameterStruct opParam = new OpParameterStruct();
                    opParam.key = Int32.Parse(result2[ctr].Value);
                    opParam.name = result2[++ctr].Value;
                    opParam.question = result2[++ctr].Value;

                    Program.log.Info(" Param key = " + opParam.key);
                    Program.log.Info(" Param name = " + opParam.name);
                    Program.log.Info(" Param question = " + opParam.question);
                    opParamList.Add(opParam);
                }
                else if (result2[ctr].Key == "Operation")
                {
                    Program.log.Info("Operation");
                    OperationStruct op = new OperationStruct();
                    op.OperationKey = opKey;
                    op.Name = name;
                    op.EstRunTime = esthours;
                    Program.log.Debug(op.OperationKey);
                    Program.log.Debug(op.Name);
                    Program.log.Debug(op.EstRunTime);

                    esthours = 0.0m;
                    opKey = "";
                    name = "";
                    machine = "";
                    estmrt = "";
                    estrun = "";
                
                    op._OpParamList = new List<OpParameterStruct>();
                    op._OpParamList.AddRange(opParamList);
                    opParamList.Clear();
                    retList.Add(op);

                    //Program.log.Info("Operation record added...");
                }
            }

            return retList;
        }

        public List<RunSectionStruct> PopulateRunSections(ListWithDuplicates result)
        {
            List<RunSectionStruct> retList = new List<RunSectionStruct>(); ;

            String sKey = "";
            String sName = "";
            int qty = 0;

            for (int ctr = 0; ctr < result.Count; ++ctr)
            {
                if (result[ctr].Key == "Key")
                {
                    sKey = result[ctr].Value;
                }
                else if (result[ctr].Key == "Name")
                {
                    sName = result[ctr].Value;
                }
                else if (result[ctr].Key == "Quantity")
                {
                    qty = Int32.Parse(result[ctr].Value);
                }
                else if (result[ctr].Key == "Pack")
                {               
                    RunSectionStruct r = new RunSectionStruct();
                    r.sectionKey = sKey;
                    r.sectionName = sName;
                    r.quantity = qty;

                    retList.Add(r);
                }
            }

            return retList;
        }
    }
}
