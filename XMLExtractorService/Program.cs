﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using log4net;
using log4net.Config;

namespace XMLExtractorService
{
    public static class Program
    {
        public static int verbosity;

        public static String sourceServer;
        public static String sourceDB;

        public static String targetServer;
        public static String targetDB;
        
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new Scheduler() 
            };
            ServiceBase.Run(ServicesToRun);
            
            XmlConfigurator.Configure();

            //Start(args);
        }

       
        public static void Start(string[] args)
        {
            log.Info("OnStart()"); 

            if (args.Length != 2)
            {            
                log.Error("Input paramaters source and target DBs not specified!");
                log.Error("  Start param:  [Source server].[Source DB] [Target server].[Target DB]");
            }
            else
            {
                String arg1 = args[0];
                String arg2 = args[1];
                
                // Parse data source
                int sep = arg1.IndexOf(".");
                sourceServer = arg1.Substring(0, sep);
                sourceDB = arg1.Substring(sep + 1);

                // Parse target source
                int sep1 = arg2.IndexOf(".");
                targetServer = arg2.Substring(0, sep1);
                targetDB = arg2.Substring(sep1 + 1);
            }   
        }

        public static void Stop()
        {            
            log.Info("OnStop()");
        }
        
    }
}
